package fr.afpa.main;

import fr.afpa.beans.*;
import java.text.DecimalFormat;

class Salaires {
	
	public static void main(String[] args){       
	
	DecimalFormat df = new DecimalFormat(".00");
		
	Personnel p = new Personnel();        
	
	System.out.println();
	p.ajouterEmploye(new Vendeur("Pierre", "Business", 45, "1995", 30000));        
	p.ajouterEmploye(new Representant("L�on", "Vendtout", 25, "2001", 20000));        
	p.ajouterEmploye(new Technicien("Yves", "Bosseur", 28, "1998", 1000));        
	p.ajouterEmploye(new Manutentionnaire("Jeanne", "Stocketout", 32, "1998", 45));        
	p.ajouterEmploye(new TechnARisque("Jean", "Flippe", 28, "2000", 1000));        
	p.ajouterEmploye(new ManutARisque("Al", "Abordage", 30, "2001", 45));       
	
	
	p.afficherSalaires();
	System.out.println();
	System.out.println("Le salaire moyen dans l'entreprise est de "+ df.format(p.salaireMoyen())+" euros.");
	
	}
}


