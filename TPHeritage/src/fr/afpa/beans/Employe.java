package fr.afpa.beans;

public abstract class Employe {

	private String nom;
	private String prenom;
	private int age;
	private String dateEntre;

	public Employe(String nom, String prenom, int age, String dateEntre) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.dateEntre = dateEntre;
	}


	public Employe() {
	}

	
	public abstract double calculerSalaire();


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getDateEntre() {
		return dateEntre;
	}

	public void setDateEntre(String dateEntre) {
		this.dateEntre = dateEntre;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
