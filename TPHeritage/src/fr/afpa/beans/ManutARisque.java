package fr.afpa.beans;

public class ManutARisque extends Manutentionnaire implements ARisque {

	public ManutARisque(String nom, String prenom, int age, String dateEntre, int heureTravailMensuel) {
		super(nom, prenom, age, dateEntre, heureTravailMensuel);
	}

	private Manutentionnaire manutention;
	
	@Override
	public double calculerSalaire() {
		double salaire = super.getHeureTravailMensuel() * 20;
		return salaire;
	}

	@Override
	public double employeARisque() {
		double prime = manutention.getSalaire() + 200;
		return prime;
	}

}
