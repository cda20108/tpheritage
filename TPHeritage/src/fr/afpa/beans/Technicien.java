package fr.afpa.beans;

public class Technicien extends Employe {

	private int nombreUnite;
	private double salaire;
	
	public Technicien(String nom, String prenom, int age, String dateEntre, int nombreUnite) {
		super(nom, prenom, age, dateEntre);
		this.nombreUnite = nombreUnite;
	}

	@Override
	public double calculerSalaire() {
		salaire = nombreUnite * 5;
		return salaire;

	}

	










	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}

	public int getNombreUnite() {
		return nombreUnite;
	}

	public void setNombreUnite(int nombreUnite) {
		this.nombreUnite = nombreUnite;
	}


	
}
