package fr.afpa.beans;

public class Vendeur extends Employe {

	private int chiffreAffaire;
	

	public Vendeur(String nom, String prenom, int age, String dateEntre, int chiffreAffaire) {
		super(nom, prenom, age, dateEntre);
		this.chiffreAffaire = chiffreAffaire;
	}














	@Override
	public double calculerSalaire() {
		double salaire = chiffreAffaire * (0.2) + 400;
		return salaire;
	}

	
	
	public int getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(int chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	
	
}
