package fr.afpa.beans;

public class Manutentionnaire extends Employe {

	private int heureTravailMensuel;
	private double salaire;
	
	
	public Manutentionnaire(String nom, String prenom, int age, String dateEntre, int heureTravailMensuel) {
		super(nom, prenom, age, dateEntre);
		this.heureTravailMensuel = heureTravailMensuel;
	}

	@Override
	public double calculerSalaire() {
		double salaire = heureTravailMensuel * 20;
		return salaire;

	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}

	public int getHeureTravailMensuel() {
		return heureTravailMensuel;
	}

	public void setHeureTravailMensuel(int heureTravailMensuel) {
		this.heureTravailMensuel = heureTravailMensuel;
	}


	
	

}
