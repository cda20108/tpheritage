package fr.afpa.beans;

public class Personnel {

	private Employe [] listeEmploye;
	private int indice;
		
	public Personnel() {
		this.listeEmploye = new Employe[6];
	}

	public void ajouterEmploye(Employe employe) {

		this.listeEmploye[this.indice] = employe;
		this.indice ++;
	}
	
	public double salaireMoyen() {
		double salaireMoyen = 0;
		
		for (int i = 0; i < listeEmploye.length; i++) {
			salaireMoyen +=  listeEmploye[i].calculerSalaire();
		}
		
		salaireMoyen = salaireMoyen/listeEmploye.length;
		
		return salaireMoyen;
		
	}

	public void afficherSalaires() {
		for (int i = 0; i < listeEmploye.length; i++) {
			System.out.println("Le salaire de " + listeEmploye[i].getNom() + " " + listeEmploye[i].getPrenom() + " est de " + listeEmploye[i].calculerSalaire());
		}
		
	}

	public Employe[] getTabPersonnel() {
		return listeEmploye;
	}

	public void setTabPersonnel(Employe[] tabPersonnel) {
		this.listeEmploye = tabPersonnel;
	}


	
	
	
	
}
