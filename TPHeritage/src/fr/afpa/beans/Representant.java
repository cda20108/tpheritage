package fr.afpa.beans;

public class Representant extends Vendeur {

	public Representant(String nom, String prenom, int age, String dateEntre, int chiffreAffaire) {
		super(nom, prenom, age, dateEntre, chiffreAffaire);
	}

	@Override
	public double calculerSalaire() {
		double salaire = super.getChiffreAffaire() * (0.2) + 800;
		return salaire;

	}

	
}
